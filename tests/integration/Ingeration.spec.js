import { shallowMount } from '@vue/test-utils'
import Customization from '../../src/components/Customization.vue'
import TeamBoi from '../../src/components/TeamBoi'
import Vuex from 'vuex'
import store from '../../src/store/index'
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)
Vue.use(Vuex)

describe('Customization.vue and TeamBoi.vue', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallowMount(Customization, {
			store
		})
	}
	)

	it('Customization Renders', () => {
		expect(wrapper.exists()).toBe(true)
	})

	it('Check for TeamBoi when valid', () => {
		wrapper.vm.$store.state.numTeams = 2
		const teamBois = wrapper.findAll(TeamBoi)
		expect(teamBois.length).toBe(2)
		expect(wrapper.find('#cust').exists()).toBe(true)
		expect(wrapper.find('#validCust').exists()).toBe(true)
	})

	it('Check for TeamBoi when not valid', () => {
		store.state.numTeams = 101
		wrapper = shallowMount(Customization, {
			store
		})
		const teamBois = wrapper.findAll(TeamBoi)
		expect(teamBois.length).toBe(1)
		expect(wrapper.find('#validCust').exists()).toBe(false)
	})
})
