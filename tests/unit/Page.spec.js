import { shallowMount } from '@vue/test-utils'
import Page from '../../src/components/Page.vue'
import Vuex from 'vuex'
import store from '../../src/store/index'
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)
Vue.use(Vuex)

describe('Page.vue', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallowMount(Page, {
			store
		})
	}
	)

	it('Page Renders', () => {
		expect(wrapper.exists()).toBe(true)
	})

	it('generate teams', () => {
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, {
			value: 'Bob',
			addNext: false
		}]
		wrapper.vm.$store.state.numTeams = 2
		wrapper.vm.$store.state.teams = [{ teamName: 'Team 1', people: [], numberOfPeople: 2 }, {
			teamName: 'Team 2',
			people: [],
			numberOfPeople: 1
		}]
		wrapper.vm.generateButton()
		expect(wrapper.vm.$store.state.teams[0].people.length).toBe(2)
		expect(wrapper.vm.$store.state.teams[1].people.length).toBe(1)
	})

	it('Back button from page 2', () => {
		wrapper.vm.currentPage = 2
		wrapper.vm.$store.state.names = []
		wrapper.vm.backButton()
		expect(wrapper.vm.currentPage).toBe(1)
		expect(wrapper.vm.$store.state.names).toStrictEqual([{
			value: '',
			addNext: true
		}])
	})

	it('Back button from page 3', () => {
		wrapper.vm.currentPage = 3
		wrapper.vm.backButton()
		expect(wrapper.vm.currentPage).toBe(2)
	})

	it('Next button from page 1', () => {
		wrapper.vm.currentPage = 1
		wrapper.vm.nextButton()
		expect(wrapper.vm.currentPage).toBe(2)
	})
})
