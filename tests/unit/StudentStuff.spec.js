import { shallowMount } from '@vue/test-utils'
import StudentStuff from '../../src/components/StudentStuff.vue'
import Vuex from 'vuex'
import store from '../../src/store/index'
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)
Vue.use(Vuex)

describe('StudentStuff.vue', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallowMount(StudentStuff, {
			store
		})
	}
	)

	it('Page Renders', () => {
		expect(wrapper.exists()).toBe(true)
	})

	it('Clear Input', () => {
		wrapper.vm.clearInput()
		expect(wrapper.vm.$refs.fileUpload.value).toBe('')
	})

	it('Parse File', () => {
		wrapper.vm.parseFile('names.csv', [new File([], 'names.csv')])
		expect(wrapper.vm.$store.state.names.length).toBeGreaterThan(0)
	})

	it('Parse Empty File', () => {
		wrapper.vm.parseFile('names.csv', [])
		expect(wrapper.vm.$store.state.names.length).toBe(1)
	})

	it('Headers Selected', () => {
		wrapper.vm.headersSelected = true
		wrapper.vm.parseFile('names.csv', [new File([], 'names.csv')])
		expect(wrapper.vm.$store.state.names.length).toBe(1)
	})

	it('Has Names', () => {
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, { value: 'Bob', addNext: false }]
		wrapper.vm.headersSelected = true
		wrapper.vm.parseFile('names.csv', [new File([], 'names.csv')])
		expect(wrapper.vm.$store.state.names.length).toBe(3)
	})
})
