import { shallowMount, createLocalVue } from '@vue/test-utils'
import NameInputs from '../../src/components/NameInputs.vue'
import Vuex from 'vuex'
import store from '../../src/store/index'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('NameInputs.vue', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallowMount(NameInputs, {
			localVue,
			store
		})
	}
	)

	it('NameInputs Renders', () => {
		expect(wrapper.exists()).toBe(true)
	})

	it('Add input', () => {
		const input = { value: 'Joe', addNext: true }
		wrapper.vm.addInput(input)
		expect(wrapper.vm.names.length).toBe(2)
	})

	it('Add False input', () => {
		const input = { value: 'Joe', addNext: false }
		wrapper.vm.addInput(input)
		expect(wrapper.vm.names.length).toBe(2)
	})

	it('Press Enter', () => {
		const event = { target: { nextElementSibling: { focus: () => {} } } }
		const input = { value: 'Bob', addNext: true }
		wrapper.vm.pressEnter(event, input)
		expect(wrapper.vm.names.length).toBe(3)
	})

	it('Check for blank true', () => {
		const input = { value: '', addNext: false }
		wrapper.vm.addInput(input)
		const index = 2
		wrapper.vm.checkForBlank(input, index)
		expect(wrapper.vm.names.length).toBe(2)
	})

	it('Check for blank false', () => {
		const input = { value: 'Willy', addNext: false }
		wrapper.vm.addInput(input)
		const index = 3
		wrapper.vm.checkForBlank(input, index)
		expect(wrapper.vm.names.length).toBe(2)
	})
})
