import { shallowMount } from '@vue/test-utils'
import Customization from '../../src/components/Customization.vue'
import Vuex from 'vuex'
import store from '../../src/store/index'
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)
Vue.use(Vuex)

describe('Customization.vue', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallowMount(Customization, {
			store
		})
	}
	)

	it('Customization Renders', () => {
		expect(wrapper.exists()).toBe(true)
	})

	it('Check valid', () => {
		wrapper.vm.$store.state.numTeams = 2
		expect(wrapper.vm.isValid()).toBe(true)
	})

	it('Check not valid small', () => {
		wrapper.vm.$store.state.numTeams = -1
		expect(wrapper.vm.isValid()).toBe(false)
	})

	it('Check not valid big', () => {
		wrapper.vm.$store.state.numTeams = 101
		expect(wrapper.vm.isValid()).toBe(false)
	})

	it('Updates teams numTeams < names.length', () => {
		wrapper.vm.$store.state.numTeams = 5
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, { value: 'Bob', addNext: false }, { value: 'George', addNext: false }, { value: 'Phil', addNext: false }, { value: 'Natalie', addNext: false }, { value: 'Leah', addNext: false }, { value: 'Terra', addNext: false }]
		wrapper.vm.updateTeams()
		expect(wrapper.vm.$store.state.teams.length).toBe(5)
	})

	it('Updates teams with teams', () => {
		wrapper.vm.$store.state.numTeams = 5
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, { value: 'Bob', addNext: false }, { value: 'George', addNext: false }, { value: 'Phil', addNext: false }, { value: 'Natalie', addNext: false }, { value: 'Leah', addNext: false }, { value: 'Terra', addNext: false }]
		wrapper.vm.$store.state.teams = [{ teamName: 'Team 1', people: [], numberOfPeople: -1 }]
		wrapper.vm.updateTeams()
		expect(wrapper.vm.$store.state.teams.length).toBe(5)
	})

	it('Updates teams numTeams > names.length', () => {
		wrapper.vm.$store.state.numTeams = 11
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, { value: 'Bob', addNext: false }, { value: 'George', addNext: false }, { value: 'Phil', addNext: false }, { value: 'Natalie', addNext: false }, { value: 'Leah', addNext: false }, { value: 'Terra', addNext: false }]
		wrapper.vm.updateTeams()
		expect(wrapper.vm.$store.state.teams.length).toBe(11)
	})

	it('Updates teams numTeams = names.length', () => {
		wrapper.vm.$store.state.numTeams = 2
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, { value: 'Bob', addNext: false }]
		wrapper.vm.$store.state.teams = [{ teamName: 'Team 1', people: [], numberOfPeople: -1 }, { teamName: 'Team 2', people: [], numberOfPeople: -1 }]
		wrapper.vm.updateTeams()
		expect(wrapper.vm.$store.state.teams.length).toBe(2)
	})

	it('Updates teams numTeams < teams.length', () => {
		wrapper.vm.$store.state.numTeams = 2
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, { value: 'Bob', addNext: false }]
		wrapper.vm.$store.state.teams = [{ teamName: 'Team 1', people: [], numberOfPeople: -1 }, { teamName: 'Team 2', people: [], numberOfPeople: -1 }, { teamName: 'Team 3', people: [], numberOfPeople: -1 }]
		wrapper.vm.updateTeams()
		expect(wrapper.vm.$store.state.teams.length).toBe(2)
	})

	it('Updates teams not valid', () => {
		wrapper.vm.$store.state.numTeams = 101
		wrapper.vm.$store.state.names = [{ value: 'Will', addNext: false }, { value: 'Joe', addNext: false }, { value: 'Bob', addNext: false }]
		wrapper.vm.$store.state.teams = [{ teamName: 'Team 1', people: [], numberOfPeople: -1 }, { teamName: 'Team 2', people: [], numberOfPeople: -1 }, { teamName: 'Team 3', people: [], numberOfPeople: -1 }]
		wrapper.vm.updateTeams()
		expect(wrapper.vm.$store.state.teams.length).toBe(3)
	})
})
