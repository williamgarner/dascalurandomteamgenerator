import { shallowMount } from '@vue/test-utils'
import TeamDetails from '../../src/components/TeamDetails.vue'
import Vuex from 'vuex'
import store from '../../src/store/index'
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)
Vue.use(Vuex)

describe('TeamDetails.vue', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallowMount(TeamDetails, {
			store
		})
	}
	)

	it('TeamDetails Renders', () => {
		expect(wrapper.exists()).toBe(true)
	})
})
