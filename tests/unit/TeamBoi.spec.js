import { shallowMount, createLocalVue } from '@vue/test-utils'
import TeamBoi from '../../src/components/TeamBoi.vue'
import Vuex from 'vuex'
import store from '../../src/store/index'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('TeamBoi.vue', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallowMount(TeamBoi, {
			localVue,
			store,
			propsData: {
				background: '#303030',
				shadow: true
			}
		})
	}
	)

	it('Team Boi Renders', () => {
		expect(wrapper.exists()).toBe(true)
	})

	it('Check for background prop', () => {
		expect(wrapper.props().background).toBe('#303030')
	})

	it('Check for shadow prop', () => {
		expect(wrapper.props().shadow).toBe(true)
	})

	it('Background shadow', () => {
		expect(wrapper.vm.style).toBe('background: #303030; box-shadow: .5rem .5rem .75rem #000000;')
	})

	it('Background no shadow', () => {
		wrapper = shallowMount(TeamBoi, {
			localVue,
			store,
			propsData: {
				background: '#303030',
				shadow: false
			}
		})

		expect(wrapper.props().background).toBe('#303030')
		expect(wrapper.props().shadow).toBe(false)
		expect(wrapper.vm.style).toBe('background: #303030; margin: 1.5rem 0')
	})
})
