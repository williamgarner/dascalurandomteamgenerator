module.exports = {
	preset: '@vue/cli-plugin-unit-jest/presets/default',
	collectCoverage: true,
	moduleFileExtensions: [
		'js',
		'json',
		'vue'
	],
	transform: {
		'^.+\\.js$': 'babel-jest',
		'^.+\\.vue$': 'vue-jest'
	},
	testMatch: [
		'**/tests/**/**/*.spec.[jt]s?(x)',
		'**/__tests__/*.[jt]s?(x)'
	]
}
